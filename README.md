# ⚠️ This project is abandoned ⚠️
This extension was originally protest-ware to the original creator of the extension due to them selling it and the company making it closed source.\
I no longer really use ChatGPT myself, so this project has sat abandoned.

# **Begin old Readme**
# ChatGPT for Search

> This project is a fork of the [ChatGPT for Google](https://github.com/wong2/chatgpt-google-extension) extension\
> That project's GitHub is no longer maintained as the extension got acquired by a company

You can downloaded it directly from the [Mozilla Store](https://addons.mozilla.org/en-US/firefox/addon/chatgpt-for-searching/)

## What this fork adds to the table

- Fully open source (and that's never gonna change)
- Better support for SearX and SearXNG
- Licence change (from GPL to LGPL)
- Removed tracking from the original project (including the constant asking if you wanna rate the extension)

### Credits to the original ChatGPT for Google extension

---

A browser extension to display ChatGPT response alongside Google (and other search engines) results

## Supported Search Engines

- SearX
- SearXNG

This extension now supports most domains for SearX(NG) from the [official searx(ng) instance tracking website](https://searx.space/).\
Plan is to support all of them soon and the ability to add your own instance in the config as well.

### Other engines

- Google
- Baidu
- Bing
- DuckDuckGo
- Brave
- Yahoo
- Naver
- Yandex
- Kagi

## Screenshot

![Screenshot](screenshots/extension.png?raw=true)

## Features

- Supports all popular search engines
- Supports the official OpenAI API
- Supports ChatGPT Plus
- Markdown rendering
- Code highlights
- Dark mode
- Provide feedback to improve ChatGPT
- Copy to clipboard
- Custom trigger mode
- Switch languages
- No more tracking from the extension
- No constantly asking for rating on web-store
- Fully FOSS (and that's never gonna change)

## Troubleshooting

### How to make it work in Brave

![Screenshot](screenshots/brave.png?raw=true)

Disable "Prevent sites from fingerprinting me based on my language preferences" in `brave://settings/shields`

## Build from source

1. `git clone https://gitlab.com/coolGi/chatgpt-search-extension.git` (Clones the repo)
2. `cd chatgpt-search-extension` (Goes into the repo's folder)
3. `npm install` (Installs dependencies)
4. `npm run build` (Builds project)
5. Load `build/chromium/` or `build/firefox/` directory to your browser

### To test on firefox

1. Do the steps in the build from source section (no need to do step 5 tough)
2. `npm run start:firefox` (runs a new instance of firefox with the extention)
