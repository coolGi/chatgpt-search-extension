import Browser from 'webextension-polyfill'
import { Theme } from './config'

export function detectSystemColorScheme() {
  if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    return Theme.Dark
  }
  return Theme.Light
}

export function getExtensionVersion() {
  return Browser.runtime.getManifest().version
}

export function getGitLocation() {
  // If you want to fork this project then change this
  return 'https://gitlab.com/coolGi/chatgpt-search-extension'
}
